# Wnioski po Aishiteru

* musi być dobry kontakt z osobą ogarniającą wejściówki na początku konwentu, nie może być tak że nie wiadomo do kogo dzwonić albo wiadomo ale ta osoba nie odbiera.
* salka nie może być łączona ze sleepem, wygląda to źle, śmierdzi i sprawia, że ludzie nie chcą tam wchodzić
* jeśli mimo wszystko salka miałaby być połączona ze sleepem to musi być dostatecznie duża, wyraźnie podzielona i sleep powinien zajmować jej mniejszą część lub być składany na dzień
* w grafiku powinna być przewidziana osoba do utrzymania sali w porządku, widać że nie dzieje się to samo w z siebie
* trzeba napisać jakiś protokół przyjęcia maina żeby sformalizować to czego oczekujemy a jednocześnie będzie to checklista żeby o niczym nie zapomnieć
* podobnie można by spisać protokół przygotowania salki (na Aishiteru zabrakło np. działającej przejściówki VGA/HDMI czy przygotowanej scenki)
* jeśli w konwenciku dodajemy długą atrakcję trzeba ją podzielić na mniejsze części, bo później jak ktoś patrzy na to co może zrobić o godzinie 13 to nie widzi WD które rozpoczęło się o 8
* trzeba pomyśleć nad jakimiś proaktywnymi działaniami na czas kiedy nie ma ludzi którzy chcą się nagrywać - może być to chodzenie po konwencie i zgarnianie ludzi, ale może też być coś innego
* może trzeba kupić jakiś stojaczek na wizytówki
* rollup powinien mieć duże hasło zachęcające do nagrywania - wyraźnie sugerujące że możesz tu wejść i się nagrać. Wtedy będziemy bardziej odporni na takie sytuacje, że jesteś wciśnięci gdzieś w kąt i nie wiadomo za bardzo co u nas się dzieje
* cały czas powracający problem; trzeba pilnować co o nas piszą bo często piszą kiepsko, z błędami etc. Jeśli chodzi o teksty medialne najlepiej prosić o autoryzację, jeśli chodzi o program to po prostu trzeba sprawdzać i krzyczeć.
